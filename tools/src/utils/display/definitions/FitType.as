package utils.display.definitions
{
    public class FitType
    {
        public static const INNER       :String = "inner";
        public static const OUTER       :String = "outer";
    }
}