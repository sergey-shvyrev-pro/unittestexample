package utils.display
{
    import flash.display.Shape;

    public class RectangleShape extends Shape
	{

        public function RectangleShape( x:Number = 0, y:Number = 0, width:Number = 100, height:Number = 100, fillColor:uint = 0, fillAlpha:Number = 1, lineThickness:Number = 0, lineColor:uint = 0, lineAlpha:Number = 1 )
		{
            if ( lineThickness > 0 )
                graphics.lineStyle ( lineThickness, lineColor, lineAlpha );

            graphics.beginFill ( fillColor, fillAlpha );
            graphics.drawRect ( x, y, width, height );
        }
   }

}