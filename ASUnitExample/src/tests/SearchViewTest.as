/**
 * Created with IntelliJ IDEA.
 * User: Sergey
 * Date: 14.03.14
 * Time: 5:53
 * To change this template use File | Settings | File Templates.
 */
package tests {
import asunit.asserts.assertNotNull;
import asunit.asserts.assertSame;
import asunit.asserts.assertTrue;
import asunit.framework.IAsync;

import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;

use namespace assertSame;

use namespace assertNotNull;

use namespace assertTrue;

public class SearchViewTest {

    [Inject]
    public var context:Sprite;

    [Inject]
    public var async:IAsync;

    private var searchView:SearchView;

    [Before]
    public function setUp():void
    {
        searchView=new SearchView();
        context.addChild(searchView);
    }

    [After]
    public function tearDown():void
    {
        searchView=null;
    }

    [Test]
    public function instantiated():void
    {
        assertTrue(searchView is SearchView);
    }

    [Test]
    public function search_view_has_search_textinput():void
    {
        assertNotNull(searchView.searchRequest_txt);
    }

    [Test]
    public function search_view_has_submitbutton():void
    {
        assertNotNull(searchView.submit_btn);
    }

    [Test]
    public function search_text_is_populated():void
    {
        searchView.searchRequest_txt.text="Hello World";
        assertSame(searchView.searchRequest_txt.text, "Hello World");
    }

    [Test]
    public function submit_button_dispatches_specific_eventname():void
    {
        searchView.addEventListener("submitSearchEvent", async.add(submitDispatchesSpecificEventType));
        searchView.submit_btn.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
    }

    protected function submitDispatchesSpecificEventType(event:Event):void
    {
        assertSame(event.type, "submitSearchEvent");
    }


    public function SearchViewTest() {
    }
}
}
